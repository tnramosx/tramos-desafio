from flask import Flask, request

app = Flask(__name__)

@app.route('/', methods=['GET', 'POST'])
def hello():
    payload = request.get_json()
    print(payload)

    payload = payload['freelance'] if payload['freelance'] else None

    if payload:
        professionalExperiences = payload.get('professionalExperiences') if payload.get('professionalExperiences') else None

        lista = [{"id":tecn['id'], "name": tecn['name'], "durationInMonths": "teste" }  for skills in professionalExperiences for tecn in skills['skills']]

        print(lista)


        response = {
            'professionalExperiences' : lista,
        }

    return response

if __name__ == '__main__':
    app.run(debug=True)